description: Testing lsmcli VOLUME tools.

/volume_access_group_success:
  description: Tests 'lsmcli volume-access-group'.
  tier: 1
  test: volume_access_group.py
  requires_setup+:
    - lsm/setup/create_volume
    - lsm/setup/create_access_group
    - lsm/setup/mask_volume

/volume_access_group_fail:
  description: Tests 'lsmcli volume-access-group' failures.
  tier: 2
  test: volume_access_group.py

/volume_cache_info_success:
  description: Tests 'lsmcli volume-cache-info'.
  tier: 1
  test: volume_cache_info.py
  requires_setup+: [lsm/setup/create_volume]
  tags: [sim] # not supported on ontap

/volume_cache_info_fail:
  description: Tests 'lsmcli volume-cache-info' failures.
  tier: 2
  test: volume_cache_info.py
  tags: [sim] # not supported on ontap

/volume_create_success:
  description: Tests 'lsmcli volume-create' (and 'lsmcli volume-delete' to clean up).
  tier: 1
  test: volume_create.py
  tags+: [smispy]

/volume_create_fail:
  description: Tests 'lsmcli volume-create' failures.
  tier: 2
  test: volume_create.py
  requires_setup+: [lsm/setup/create_volume]
  tags+: [smispy]

/volume_delete_fail:
  description: Tests 'lsmcli volume-delete' failures.
  tier: 2
  test: volume_delete.py
  requires_setup+: [lsm/setup/create_volume]
  tags+: [smispy]

/volume_dependants_success:
  description: Tests 'lsmcli volume-dependants'.
  tier: 1
  test: volume_dependants.py
  requires_setup+:
    - lsm/setup/create_volume
    - lsm/setup/create_volume_2
  tags: [sim] # useless on ontap as no dependency can be created there

/volume_dependants_fail:
  description: Tests 'lsmcli volume-dependants' failures.
  tier: 2
  test: volume_dependants.py
  tags: [sim] # useless on ontap as no dependency can be created there

/volume_dependants_rm_success:
  description: Tests 'lsmcli volume-dependants-rm'.
  tier: 1
  test: volume_dependants_rm.py
  requires_setup+:
    - lsm/setup/create_volume
    - lsm/setup/create_volume_2
  tags: [sim] # useless on ontap as no dependency can be created there

/volume_dependants_rm_fail:
  description: Tests 'lsmcli volume-dependants-rm' failures.
  tier: 2
  test: volume_dependants_rm.py
  tags: [sim] # useless on ontap as no dependency can be created there

/volume_enable_success:
  description: Tests 'lsmcli volume-enable' (and 'lsmcli volume-disable' to clean up).
  tier: 1
  test: volume_enable.py
  requires_setup+: [lsm/setup/create_volume]

/volume_enable_fail:
  description: Tests 'lsmcli volume-enable' failures.
  tier: 2
  test: volume_enable.py
  requires_setup+: [lsm/setup/create_volume]

/volume_disable_fail:
  description: Tests 'lsmcli volume-disable' failures.
  tier: 2
  test: volume_disable.py
  requires_setup+: [lsm/setup/create_volume]

/volume_mask_success:
  description: Tests 'lsmcli volume-mask' (and 'lsmcli volume-unmask' to clean up).
  tier: 1
  test: volume_mask.py
  requires_setup+:
    - lsm/setup/create_volume
    - lsm/setup/create_access_group

/volume_mask_fail:
  description: Tests 'lsmcli volume-mask' failures.
  tier: 2
  test: volume_mask.py
  requires_setup+:
    - lsm/setup/create_volume
    - lsm/setup/create_access_group

/volume_unmask_fail:
  description: Tests 'lsmcli volume-unmask' failures.
  tier: 2
  test: volume_unmask.py
  requires_setup+:
    - lsm/setup/create_volume
    - lsm/setup/create_access_group

/volume_ident_led_success:
  description: Tests 'lsmcli volume-ident-led-on' and 'lsmcli volume-ident-led-off'.
  tier: 1
  test: volume_ident_led.py
  requires_setup+: [lsm/setup/create_volume]
  tags: [sim] # not supported on ontap

/volume_ident_led_fail:
  description: Tests 'lsmcli volume-ident-led-on' and 'lsmcli volume-ident-led-off' failures.
  tier: 2
  test: volume_ident_led.py
  requires_setup+: [lsm/setup/create_volume]
  tags: [sim] # not supported on ontap

/volume_phy_disk_cache_update_success:
  description: Tests 'lsmcli volume-phy-disk-cache-update'.
  tier: 1
  test: volume_phy_disk_cache_update.py
  requires_setup+: [lsm/setup/create_volume]
  tags: [sim] # not supported on ontap

/volume_phy_disk_cache_update_fail:
  description: Tests 'lsmcli volume-phy-disk-cache-update' failures.
  tier: 2
  test: volume_phy_disk_cache_update.py
  requires_setup+: [lsm/setup/create_volume]
  tags: [sim] # not supported on ontap

/volume_read_cache_policy_update_success:
  description: Tests 'lsmcli volume-read-cache-policy-update'.
  tier: 1
  test: volume_read_cache_policy_update.py
  requires_setup+: [lsm/setup/create_volume]
  tags: [sim] # not supported on ontap

/volume_read_cache_policy_update_fail:
  description: Tests 'lsmcli volume-read-cache-policy-update' failures.
  tier: 2
  test: volume_read_cache_policy_update.py
  requires_setup+: [lsm/setup/create_volume]
  tags: [sim] # not supported on ontap

/volume_replicate_success:
  description: Tests 'lsmcli volume-replicate'.
  tier: 1
  test: volume_replicate.py
  requires_setup+: [lsm/setup/create_volume]

/volume_replicate_fail:
  description: Tests 'lsmcli volume-replicate' failures.
  tier: 2
  test: volume_replicate.py
  requires_setup+: [lsm/setup/create_volume]

/volume_replicate_range_success:
  description: Tests 'lsmcli volume-replicate-range'.
  tier: 1
  test: volume_replicate_range.py
  requires_setup+:
    - lsm/setup/create_volume
    - lsm/setup/create_volume_2

/volume_replicate_range_fail:
  description: Tests 'lsmcli volume-replicate-range' failures.
  tier: 2
  test: volume_replicate_range.py
  requires_setup+:
    - lsm/setup/create_volume
    - lsm/setup/create_volume_2

/volume_replicate_range_block_size_success:
  description: Tests 'lsmcli volume-replicate-range-block-size'.
  tier: 1
  test: volume_replicate_range_block_size.py

/volume_replicate_range_block_size_fail:
  description: Tests 'lsmcli volume-replicate-range-block-size' failures.
  tier: 2
  test: volume_replicate_range_block_size.py

/volume_write_cache_policy_update_success:
  description: Tests 'lsmcli volume-write-cache-policy-update'.
  tier: 1
  test: volume_write_cache_policy_update.py
  requires_setup+: [lsm/setup/create_volume]
  tags: [sim] # not supported on ontap

/volume_write_cache_policy_update_fail:
  description: Tests 'lsmcli volume-write-cache-policy-update' failures.
  tier: 2
  test: volume_write_cache_policy_update.py
  requires_setup+: [lsm/setup/create_volume]
  tags: [sim] # not supported on ontap
